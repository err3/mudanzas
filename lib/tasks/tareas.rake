task rellenar_movidas: :environment do
  Status.where(name: "Usado").first.update_attribute(:name, "Bueno")
  Status.where(name: "Malas Condiciones").first.update_attribute(:name, "Nuevo")
  Status.where(name: "Nuevo").first.update_attribute(:name, "Regular")
end