# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130919214836) do

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "categories", ["name"], :name => "index_categories_on_name", :unique => true

  create_table "locations", :force => true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "postal_code"
    t.string   "city"
    t.string   "province"
    t.string   "country"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "material_models", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "furniture"
    t.string   "measures"
    t.string   "luggage"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer  "location_headquarter_id"
    t.string   "photo2_file_name"
    t.string   "photo2_content_type"
    t.integer  "photo2_file_size"
    t.datetime "photo2_updated_at"
    t.string   "photo3_file_name"
    t.string   "photo3_content_type"
    t.integer  "photo3_file_size"
    t.datetime "photo3_updated_at"
    t.string   "model_type"
    t.string   "characteristics"
    t.string   "color"
    t.string   "unit"
  end

  add_index "material_models", ["name"], :name => "index_material_models_on_name", :unique => true

  create_table "materials", :force => true do |t|
    t.string   "barcode"
    t.integer  "status_id"
    t.integer  "category_id"
    t.integer  "porterage_id"
    t.integer  "shelf_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "material_model_id"
    t.integer  "location_id"
    t.string   "shelf_part"
    t.string   "zone"
  end

  add_index "materials", ["barcode"], :name => "index_materials_on_barcode", :unique => true

  create_table "porterages", :force => true do |t|
    t.date     "enter_date"
    t.date     "exit_date"
    t.integer  "origin_location"
    t.integer  "destiny_location"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
    t.string   "operator"
  end

  add_index "porterages", ["enter_date"], :name => "index_porterages_on_enter_date"
  add_index "porterages", ["exit_date"], :name => "index_porterages_on_exit_date"

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_users", :force => true do |t|
    t.integer  "role_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shelves", :force => true do |t|
    t.string   "name"
    t.integer  "location_shed_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "shelves", ["name"], :name => "index_shelves_on_name"

  create_table "statuses", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "name"
    t.string   "last_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
