class AddAttachmentPhoto3ToMaterialModels < ActiveRecord::Migration
  def self.up
    change_table :material_models do |t|
      t.attachment :photo3
    end
  end

  def self.down
    drop_attached_file :material_models, :photo3
  end
end
