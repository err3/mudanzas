class CreateMaterials < ActiveRecord::Migration
  def change
    create_table :materials do |t|
      t.string :barcode

      #Relaciones
      t.integer :status_id #Estado
      t.integer :category_id #Categoría
      t.integer :porterage_id #Porte
      t.integer :location_headquarter_id #Sede
      t.integer :shelf_id #Estantería

      t.timestamps
    end

    add_index :materials, :barcode, :unique => true
  end
end
