class AddHeadquarterIdToMaterialModels < ActiveRecord::Migration
  def change
    remove_column :materials, :location_headquarter_id
    add_column :material_models, :location_headquarter_id, :integer
  end
end
