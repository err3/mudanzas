class AddMaterialModelIdToMaterials < ActiveRecord::Migration
  def change
    add_column :materials, :material_model_id, :integer
  end
end
