class CreateShelves < ActiveRecord::Migration
  def change
    create_table :shelves do |t|
      t.string :name
      t.integer :location_shed

      t.timestamps
    end

    add_index :shelves, :name
  end
end
