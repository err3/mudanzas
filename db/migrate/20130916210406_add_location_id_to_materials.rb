class AddLocationIdToMaterials < ActiveRecord::Migration
  def change
    add_column :materials, :location_id, :integer
  end
end
