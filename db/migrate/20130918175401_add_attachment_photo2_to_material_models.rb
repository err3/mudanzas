class AddAttachmentPhoto2ToMaterialModels < ActiveRecord::Migration
  def self.up
    change_table :material_models do |t|
      t.attachment :photo2
    end
  end

  def self.down
    drop_attached_file :material_models, :photo2
  end
end
