class AddMuchasMierdasToMaterialModels < ActiveRecord::Migration
  def change
    add_column :material_models, :model_type, :string
    add_column :material_models, :characteristics, :string
    add_column :material_models, :color, :string
  end
end
