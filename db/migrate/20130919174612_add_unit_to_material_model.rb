class AddUnitToMaterialModel < ActiveRecord::Migration
  def change
    add_column :material_models, :unit, :string
  end
end
