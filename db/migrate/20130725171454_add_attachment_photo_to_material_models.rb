class AddAttachmentPhotoToMaterialModels < ActiveRecord::Migration
  def self.up
    change_table :material_models do |t|
      t.attachment :photo
    end
  end

  def self.down
    drop_attached_file :material_models, :photo
  end
end
