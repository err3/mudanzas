class RenameLocationShed < ActiveRecord::Migration
  def change
    rename_column :shelves, :location_shed, :location_shed_id
  end
end
