class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.string :address
      t.string :postal_code
      t.string :city
      t.string :province
      t.string :country
      t.string :type

      t.timestamps
    end
  end
end
