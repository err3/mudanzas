class CreateMaterialModels < ActiveRecord::Migration
  def change
    create_table :material_models do |t|
      t.string :name
      t.text   :description
      t.string :furniture
      t.string :measures
      t.string :luggage

      t.timestamps
    end
    add_index :material_models, :name, :unique => true
  end
end
