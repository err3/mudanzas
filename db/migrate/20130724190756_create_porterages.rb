class CreatePorterages < ActiveRecord::Migration
  def change
    create_table :porterages do |t|
      t.date :enter_date
      t.date :exit_date

      t.integer :origin_location
      t.integer :destiny_location

      t.timestamps
    end
    add_index :porterages, :enter_date
    add_index :porterages, :exit_date
  end
end
