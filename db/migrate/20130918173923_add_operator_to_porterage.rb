class AddOperatorToPorterage < ActiveRecord::Migration
  def change
    add_column :porterages, :operator, :string
  end
end
