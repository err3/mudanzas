require "rvm/capistrano"
require "bundler/capistrano"
require './config/deploy_tasks/after_deploy'

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

set :rvm_type, :user

set :scm, :git
#set :deploy_via, :remote_cache

set :user, "mudanzas"
set :use_sudo, false

set :application, "mudanzasdelafuente"
set :repository, "git@bitbucket.org:err3/mudanzas.git"
role :web, "87.106.173.221"
role :app, "87.106.173.221"
role :db, "87.106.173.221", primary: true
set :rvm, 1
set :rails_env, "production"
set :branch, "master"
set :bundle_without, [:development]
set :deploy_to, "/home/mudanzas/apps/#{application}"

set :unicorn_binary, "bundle exec unicorn_rails"
set :unicorn_config, "#{current_path}/config/unicorn.rb"
set :unicorn_pid,    "#{current_path}/tmp/pids/unicorn.pid"

namespace :solr do
  desc "start solr"
  task :start, :roles => :app, :except => { :no_release => true } do
    run "cd #{current_path} && RAILS_ENV=#{rails_env} bundle exec sunspot-solr start --port=8983 --data-directory=#{shared_path}/solr/data --pid-dir=#{shared_path}/pids"
  end
  desc "stop solr"
  task :stop, :roles => :app, :except => { :no_release => true } do
    run "cd #{current_path} && RAILS_ENV=#{rails_env} bundle exec sunspot-solr stop --port=8983 --data-directory=#{shared_path}/solr/data --pid-dir=#{shared_path}/pids"
  end
  desc "reindex the whole database"
  task :reindex, :roles => :app do
    stop
    run "rm -rf #{shared_path}/solr/data"
    start
    run "cd #{current_path} && RAILS_ENV=#{rails_env} bundle exec rake sunspot:solr:reindex"
  end
  desc "Symlink in-progress deployment to a shared Solr index"
  task :symlink, :except => { :no_release => true } do
    run "ln -s #{shared_path}/solr/data #{current_path}/solr/data"
    run "ln -s #{shared_path}/solr/pids #{current_path}/solr/pids"
  end
end

namespace :deploy do
  task :setup_solr_data_dir do
    run "mkdir -p #{shared_path}/solr/data"
  end

  task :start, :roles => :app, :except => { :no_release => true } do
    run "cd #{current_path} && #{try_sudo} #{unicorn_binary} -c #{unicorn_config} -E #{rails_env} -D"
  end
  task :stop, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} kill `cat #{unicorn_pid}`"
  end
  task :graceful_stop, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} kill -s QUIT `cat #{unicorn_pid}`"
  end
  task :reload, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} kill -s USR2 `cat #{unicorn_pid}`"
  end
  task :restart, :roles => :app, :except => { :no_release => true } do
    stop
    start
  end
end

# -- Stuff to do after deploy
after "deploy:update_code", "deploy:symlink_config", "deploy:cleanup"#, "ts:arrancar"
after "deploy:setup","deploy:create_dirs", 'deploy:setup_solr_data_dir'
