Mudanzasdelafuente::Application.routes.draw do
  devise_for :users
  #TODO Renombrar rutas
  root to: "home#index"

  resources :material_models do
    collection do
      get :search
    end
  end

  resources :materials do
    collection do
      get :update_shed
      get :search
    end
  end

  resources :location_headquarters
  resources :location_sheds
  resources :shelves

  resources :porterages do
    collection do
      get :search
    end

    member do
      get :deliver
      get :deliver_back
      get :insert_code
      get :scan_code
      put :insert_manual_code
      get :delete_code
      put :insert_batch_codes
    end
  end

end
