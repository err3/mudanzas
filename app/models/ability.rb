class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.is_illuminadmin?
      can :manage, :all
    else
      # no permissions
    end
  end
end