class Location < ActiveRecord::Base
  has_many :porterages
  has_many :materials
  attr_accessible :name, :address, :postal_code, :city, :province, :country
end
