class LocationShed < Location
  has_many :shelves

  validates_presence_of :name
  validates_uniqueness_of :name
end