class LocationHeadquarter < Location
  has_many :material_models

  validates_presence_of :name, :address, :postal_code, :city, :province, :country

end