class Shelf < ActiveRecord::Base
  attr_accessible :name, :location_shed_id

  belongs_to :location_shed

  validates_presence_of :name, :location_shed_id
end
