class MaterialModel < ActiveRecord::Base
  attr_accessible :name, :description, :furniture, :measures, :luggage, :location_headquarter_id,
                  :photo, :photo2, :photo3, :model_type, :characteristics, :color, :unit

  validates_presence_of  :description, :furniture, :measures, :characteristics, :model_type

  has_many :materials, dependent: :destroy
  belongs_to :location_headquarter
  belongs_to :category
  has_attached_file :photo, :styles => { :thumb => "90x90", :large => "400x400"}
  has_attached_file :photo2, :styles => { :thumb => "90x90", :large => "400x400"}
  has_attached_file :photo3, :styles => { :thumb => "90x90", :large => "400x400"}

  #SolR method
  searchable do
    integer :id
    string :name
    string :description
    string :furniture
    text :model_type
    text :characteristics
    text :color
    text :unit

    integer :location_headquarter_id
  end
end
