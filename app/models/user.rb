class User < ActiveRecord::Base
  #Constantes conspirantes
  ILLUMINADMIN_ROLE = "Illuminadmin"

  # Propiedades de Devise
  devise :database_authenticatable, :rememberable, :trackable, :validatable

  # Atributos manejables desde la aplicación con mass-assignment
  attr_accessible :email, :password, :password_confirmation, :remember_me , :name, :last_name

  # Validaciones de creación de objetos
  validates_uniqueness_of :email
  validates_presence_of :name, :last_name

  has_and_belongs_to_many :roles

  def is_illuminadmin?
    has_role? ILLUMINADMIN_ROLE
  end

  private

  def has_role?(role)
    return !!self.roles.find_by_name(role.to_s.camelize)
  end
end