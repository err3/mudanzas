class Status < ActiveRecord::Base
  #ESTA MIERDA NO ES DEFINITIVA
  STATUS_USED = 1
  STATUS_NEW = 2
  STATUS_BROKEN = 3
  STATUS_BAD = 4

  attr_accessible :name

  validates_uniqueness_of :name
  validates_presence_of :name

  has_many :materials
end
