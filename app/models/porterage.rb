class Porterage < ActiveRecord::Base
  STATUS_TRANSIT = "En transito"
  STATUS_DELIVERED = "Entregado"

  attr_accessible :enter_date, :exit_date, :origin_location, :destiny_location, :status, :operator

  has_many :materials

  belongs_to :origin, class_name: 'Location', foreign_key: :origin_location
  belongs_to :destiny, class_name: 'Location', foreign_key: :destiny_location

  validates_presence_of :origin, :destiny

  before_create :default_values

  def default_values
    self.exit_date = Date.today
    self.status = STATUS_TRANSIT
  end

  searchable do
    integer :id
    text :status
    text :operator

    integer :origin do
      origin.id
    end

    text :materials do
      materials.map { |m| m.material_model.model_type }
    end

    integer :destiny do
      destiny.id
    end

    date :enter_date
    date :exit_date
  end

  def assign_batch_porterages(materials)
    materials.each do |material|
      material.assign_porterage(self)
    end
  end
end