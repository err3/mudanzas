class Material < ActiveRecord::Base
  after_create :assign_shelf_location
  require 'barby'
  require 'barby/barcode/code_128'
  require 'barby/barcode/ean_13'
  require 'barby/outputter/png_outputter'
  require 'chunky_png'

  attr_accessible :barcode, :status_id, :category_id, :porterage_id, :shelf_id, :material_model_id, :location_id, :shelf_part, :zone

  belongs_to :porterage
  belongs_to :shelf
  belongs_to :material_model
  belongs_to :location
  belongs_to :status

  validates_uniqueness_of :barcode
  validates_presence_of :location_id

  scope :material_per_location, lambda{ |location_filter| where("location_id = ?", location_filter) }
  scope :material_per_model, lambda{ |model_filter| where("material_model_id = ?", model_filter)}

  #SolR method
  searchable do
    integer :id

    integer :status do
      status.id
    end

    text :shelf do
      shelf.name
    end

    text :location_shed do
      shelf.location_shed.name
    end

    integer :model do
      material_model.id
    end

    integer :location do
      location.id
    end
  end

  def assign_porterage(porterage, type = "origin")
    self.porterage_id = porterage.id
    self.location_id = porterage.origin.id if type == "origin"
    self.location_id = porterage.destiny.id if type == "destiny"
    self.save
  end

  def assign_shelf_location
    self.location_id = self.shelf.location_shed_id
    self.save
  end

  def ean_code
    ceroisimas = 12 - id.to_s.size
    res = id.to_s

    ceroisimas.times do |cero|
      res.insert 0, "0"
    end

    res
  end

  def self.limpiar_batch(batch)
    batch = batch.split("\n")
    res = []
    batch.each do |codigo|
      codigo = codigo.to_i / 10
      res << codigo
    end

    res
  end

  def full_code
    self.material_model.id.to_s + "-" + self.id.to_s
  end

end
