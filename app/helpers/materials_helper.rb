module MaterialsHelper
  def models_list
    MaterialModel.all.map { |x| [(x.model_type + ' ' + x.characteristics + ' ' + x.description + ' - ' + x.measures) , x.id] }
  end

  def sheds_list
    LocationShed.all.map { |x| [x.name, x.id] }
  end

  def status_list
    Status.all.map { |x| [x.name, x.id] }
  end

  def model_models
    MaterialModel.all.map { |x| [(x.model_type + ' ' + x.characteristics + ' ' + x.description + ' - ' + x.measures) , x.id] }
  end

  def locations
    Location.all.map { |x| [x.name, x.id] }
  end
end
