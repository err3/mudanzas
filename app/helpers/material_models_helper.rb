module MaterialModelsHelper
  def headquarters
    arr = LocationHeadquarter.all.map { |x| [x.name, x.id] }
    arr
  end

  def categories
    arr = Category.all.map { |x| [x.name, x.id] }
    arr
  end

end