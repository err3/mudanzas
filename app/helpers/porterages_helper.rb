module PorteragesHelper
  def origins
    Location.all.map {|x| [x.name, x.id]}
  end

  def destinies
    Location.all.map {|x| [x.name, x.id]}
  end

  def material_models
    MaterialModel.all.map { |x| [(x.model_type + ' ' + x.characteristics + ' ' + x.description + ' - ' + x.measures) , x.model_type] }
  end
end