module ApplicationHelper
  def headquarter_list
    LocationHeadquarter.all
  end

  def shed_list
    LocationShed.all
  end
end
