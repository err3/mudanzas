require 'will_paginate/array'
class ShelvesController < ApplicationController
  protect_from_forgery
  load_and_authorize_resource

  def index
    @shelves = Shelf.all.paginate(page: params[:page], per_page: Mudanzasdelafuente::Application::ELEMENTS_PER_PAGE)

    respond_to do |format|
      format.html
      format.json { render json: @shelfs }
    end
  end

  def show
    @shelf = Shelf.find(params[:id])

    respond_to do |format|
      format.html
      format.json { render json: @shelf }
    end
  end

  def new
    @shelf = Shelf.new

    respond_to do |format|
      format.html
      format.json { render json: @shelf }
    end
  end

  def create
    @shelf.attributes = params[:shelf]

    respond_to do |format|
      if @shelf.save
        format.html { redirect_to shelves_path, notice: "Estanteria creada correctamente" }
        format.json { render json: @shelf, status: :created, location: @shelf }
      else
        format.html { render action: 'new' }
        format.json { render json: @shelf.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @shelf = Shelf.find(params[:id])

    respond_to do |format|
      format.html
      format.json { render json: @shelf }
    end
  end

  def update
    @shelf = Shelf.find(params[:id])

    respond_to do |format|
      if @shelf.update_attributes(params[:shelf])
        format.html { redirect_to shelves_path, notice: "Estanteria modificada correctamente" }
        format.json { render json: @shelf, status: :ok }
      else
        format.html { render action: 'edit' }
        format.json { render json: @shelf.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @shelf = Shelf.find(params[:id])
    @shelf.destroy

    respond_to do |format|
      format.html { redirect_to shelves_path, notice: "Estanteria destruida correctamente" }
      format.json { head :ok }
    end
  end

end
