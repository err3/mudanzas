require 'will_paginate/array'
class MaterialModelsController < ApplicationController
  protect_from_forgery
  load_and_authorize_resource

  def index
    res = MaterialModel.search do
      with :id, params[:code] unless params[:code].blank?
      with :description, params[:description] unless params[:description].blank?
      with :furniture, params[:furniture] unless params[:furniture].blank?
      with :location_headquarter_id, params[:location_headquarter_id] unless params[:location_headquarter_id].blank?
      fulltext params[:characteristics] unless params[:characteristics].blank?
      fulltext params[:model_type] unless params[:model_type].blank?
      fulltext params[:color] unless params[:color].blank?

      paginate per_page: 2000
    end

    @material_models = res.results.paginate(page: params[:page], per_page: Mudanzasdelafuente::Application::ELEMENTS_PER_PAGE)
    @material_models_count = res.results.count

    respond_to do |format|
      format.html
      format.json { render json: @material_models }
    end
  end

  def show
    @material_model = MaterialModel.find(params[:id])

    respond_to do |format|
      format.html
      format.json { render json: @material_model }
    end
  end

  def new
    @material_model = MaterialModel.new

    respond_to do |format|
      format.html
      format.json { render json: @material_model }
    end
  end

  def create
    @material_model.attributes = params[:material_model]

    respond_to do |format|
      if @material_model.save
        format.html { redirect_to material_models_path, notice: "Modelo creado correctamente" }
        format.json { render json: @material_model, status: :created, location: @material_model }
      else
        format.html { render action: 'new' }
        format.json { render json: @material_model.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @material_model = MaterialModel.find(params[:id])

    respond_to do |format|
      format.html
      format.json { render json: @material_model }
    end
  end

  def update
    @material_model = MaterialModel.find(params[:id])

    respond_to do |format|
      if @material_model.update_attributes(params[:material_model])
        format.html { redirect_to materials_path , notice: "Modelo modificado correctamente" }
        format.json { render json: @material_model, status: :ok }
      else
        format.html { render action: 'edit' }
        format.json { render json: @material_model.errors, status: :unprocessable_entity }
      end
    end
  end

  def search
    res = MaterialModel.search do
      with :id, params[:code] unless params[:code].blank?
      with :description, params[:description] unless params[:description].blank?
      with :furniture, params[:furniture] unless params[:furniture].blank?
      with :location_headquarter_id, params[:location_headquarter_id] unless params[:location_headquarter_id].blank?
      fulltext params[:characteristics] unless params[:characteristics].blank?
      fulltext params[:model_type] unless params[:model_type].blank?
      fulltext params[:color] unless params[:color].blank?

      paginate per_page: 2000
    end

    @material_models = res.results
  end

  def destroy
    @material_model = MaterialModel.find(params[:id])
    @material_model.destroy

    respond_to do |format|
      format.html { redirect_to material_models_path, notice: "Modelo destruido correctamente" }
      format.json { head :ok }
    end
  end

end