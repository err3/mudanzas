require 'will_paginate/array'
class LocationShedsController < ApplicationController
  protect_from_forgery
  load_and_authorize_resource

  def index
    @location_sheds = LocationShed.all.paginate(page: params[:page], per_page: Mudanzasdelafuente::Application::ELEMENTS_PER_PAGE)

    respond_to do |format|
      format.html
      format.json { render json: @location_sheds }
    end
  end

  def show
    @location_shed = LocationShed.find(params[:id])

    respond_to do |format|
      format.html
      format.json { render json: @location_shed }
    end
  end

  def new
    @location_shed = LocationShed.new

    respond_to do |format|
      format.html
      format.json { render json: @location_shed }
    end
  end

  def create
    @location_shed.attributes = params[:location_shed]

    respond_to do |format|
      if @location_shed.save
        format.html { redirect_to location_sheds_path, notice: "Nave creada correctamente" }
        format.json { render json: @location_shed, status: :created, location: @location_shed }
      else
        format.html { render action: 'new' }
        format.json { render json: @location_shed.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @location_shed = LocationShed.find(params[:id])

    respond_to do |format|
      format.html
      format.json { render json: @location_shed }
    end
  end

  def update
    @location_shed = LocationShed.find(params[:id])

    respond_to do |format|
      if @location_shed.update_attributes(params[:location_shed])
        format.html { redirect_to location_sheds_path, notice: "Nave modificada correctamente" }
        format.json { render json: @location_shed, status: :ok }
      else
        format.html { render action: 'edit' }
        format.json { render json: @location_shed.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @location_shed = LocationShed.find(params[:id])
    @location_shed.destroy

    respond_to do |format|
      format.html { redirect_to location_sheds_path, notice: "Nave destruida correctamente" }
      format.json { head :ok }
    end
  end

end
