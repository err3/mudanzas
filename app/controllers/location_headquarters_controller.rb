class LocationHeadquartersController < ApplicationController
  protect_from_forgery
  load_and_authorize_resource
  require 'will_paginate/array'
  def index
    @location_headquarters = LocationHeadquarter.all.paginate(page: params[:page], per_page: Mudanzasdelafuente::Application::ELEMENTS_PER_PAGE)

    respond_to do |format|
      format.html
      format.json { render json: @location_headquarters }
    end
  end

  def show
    @location_headquarter = LocationHeadquarter.find(params[:id])

    respond_to do |format|
      format.html
      format.json { render json: @location_headquarter }
    end
  end

  def new
    @location_headquarter = LocationHeadquarter.new

    respond_to do |format|
      format.html
      format.json { render json: @location_headquarter }
    end
  end

  def create
    @location_headquarter.attributes = params[:location_headquarter]

    respond_to do |format|
      if @location_headquarter.save
        format.html { redirect_to location_headquarters_path, notice: "Sede dada de alta correctamente" }
        format.json { render json: @location_headquarter, status: :created, location: @location_headquarter }
      else
        format.html { render action: 'new' }
        format.json { render json: @location_headquarter.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @location_headquarter = LocationHeadquarter.find(params[:id])

    respond_to do |format|
      format.html
      format.json { render json: @location_headquarter }
    end
  end

  def update
    @location_headquarter = LocationHeadquarter.find(params[:id])

    respond_to do |format|
      if @location_headquarter.update_attributes(params[:location_headquarter])
        format.html { redirect_to location_headquarters_path, notice: "Sede actualizada correctamente" }
        format.json { render json: @location_headquarter, status: :ok }
      else
        format.html { render action: 'edit' }
        format.json { render json: @location_headquarter.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @location_headquarter = LocationHeadquarter.find(params[:id])
    @location_headquarter.destroy

    respond_to do |format|
      format.html { redirect_to location_headquarters_path, notice: "Sede eliminada correctamente" }
      format.json { head :ok }
    end
  end

end
