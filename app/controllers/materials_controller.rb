require 'will_paginate/array'
class MaterialsController < ApplicationController
  protect_from_forgery
  load_and_authorize_resource

  def index

    if params[:location_filter].present?
      @materials = Material.material_per_location(params[:location_filter]).paginate(page: params[:page], per_page: Mudanzasdelafuente::Application::ELEMENTS_PER_PAGE)
      @materials_count = Material.material_per_location(params[:location_filter]).count
    elsif params[:model_filter].present?
      @materials = Material.material_per_model(params[:model_filter]).paginate(page: params[:page], per_page: Mudanzasdelafuente::Application::ELEMENTS_PER_PAGE)
      @materials_count = Material.material_per_model(params[:model_filter]).count
    else
      res = Material.search do
        with :id, params[:code] unless params[:code].blank?
        with :status, params[:status] unless params[:status].blank?
        with :model, params[:material_model_name] unless params[:material_model_name].blank?
        with :location, params[:location_name] unless params[:location_name].blank?
        fulltext [params[:shelf], params[:location_shed]].join(" ") unless params[:shelf].blank?
        paginate per_page: 2000
      end

      @materials = res.results.paginate(page: params[:page], per_page: Mudanzasdelafuente::Application::ELEMENTS_PER_PAGE)
      @materials_count = res.results.count
    end


    respond_to do |format|
      format.html
      format.json { render json: @materials }
    end
  end

  def show
    @material = Material.find(params[:id])
    @barcode = Barby::EAN13.new(@material.ean_code)
    p @barcode.inspect
    @blob = Barby::PngOutputter.new(@barcode).to_png #Raw PNG data
    File.open("public/barcodes/#{@material.id}.png", 'w'){|f| f.write @blob }
  end

  def new
    @material = Material.new
    @shelves = []

    respond_to do |format|
      format.html
      format.json { render json: @material }
      format.js
    end
  end

  def create
    number_of_materials = params[:times].to_i

    number_of_materials.times do |material|
      Material.create(params[:material])
    end

    respond_to do |format|
        format.html { redirect_to materials_path, notice: "Material creado correctamente" }
        format.json { render json: @material, status: :created, location: @material }
    end
  end

  def edit
    @material = Material.find(params[:id])
    @shelves = [[@material.shelf.name, @material.shelf.id]]

    respond_to do |format|
      format.html {}
      format.json { render json: @material }
    end

  end

  def update
    @material = Material.find(params[:id])

    respond_to do |format|
      if @material.update_attributes(params[:material])
        format.html { redirect_to materials_path, notice: "Material modificado correctamente" }
        format.json { render json: @material, status: :ok }
      else
        format.html { render action: 'edit' }
        format.json { render json: @material.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @material = Material.find(params[:id])
    @material.destroy

    respond_to do |format|
      format.html { redirect_to materials_path, notice: "Material destruido correctamente" }
      format.json { head :ok }
    end
  end

  def update_shed
    @shelves =  params[:shed_id].present? ? Shelf.where("location_shed_id = #{params[:shed_id]}") : []
    @shelves = @shelves.map { |x| [x.name, x.id] }.insert(0, "Selecciona Estanteria")
  end

  def scancode
    respond_to do |format|
      format.js
    end
  end

  def search
    res = Material.search do
      with :id, params[:code] unless params[:code].blank?
      with :status, params[:status] unless params[:status].blank?
      with :model, params[:material_model_name] unless params[:material_model_name].blank?
      with :location, params[:location_name] unless params[:location_name].blank?
      fulltext [params[:shelf], params[:location_shed]].join(" ") unless params[:shelf].blank?
      paginate per_page: 2000
    end

    @materials = res.results.paginate(page: params[:page], per_page: Mudanzasdelafuente::Application::ELEMENTS_PER_PAGE)
    @materials_count = res.results.count
  end

end
