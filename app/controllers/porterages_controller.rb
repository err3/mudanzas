require 'will_paginate/array'
class PorteragesController < ApplicationController
  protect_from_forgery
  load_and_authorize_resource

  def index
    res = Porterage.search do
      with :id, params[:code] unless params[:code].blank?
      with(:exit_date).greater_than_or_equal_to(params[:day_from].to_date) unless params[:day_from].blank?
      with(:exit_date).less_than_or_equal_to(params[:day_to].to_date) unless params[:day_to].blank?
      with :origin, params[:origin] unless params[:origin].blank?
      with :destiny, params[:destiny] unless params[:destiny].blank?
      keywords params[:model_name] unless params[:model_name].blank?
      fulltext(params[:status])

      paginate per_page: 2000
    end

    @porterages = res.results.paginate(page: params[:page], per_page: Mudanzasdelafuente::Application::ELEMENTS_PER_PAGE)
    @porterages_count = res.results.count

    respond_to do |format|
      format.html
      format.json { render json: @porterages }
    end
  end

  def show
    @porterage = Porterage.find(params[:id])
    @materials = @porterage.materials

    respond_to do |format|
      format.html
      format.json { render json: @porterage }
    end
  end

  def new
    @porterage = Porterage.new

    respond_to do |format|
      format.html
      format.json { render json: @location_shed }
    end
  end

  def create
    @porterage.attributes = params[:porterage]

    respond_to do |format|
      if @porterage.save
        format.html { redirect_to scan_code_porterage_path(@porterage), notice: "Porte creado correctamente" }
        format.json { render json: @porterage, status: :created, location: @porterage }
      else
        format.html { render action: 'new' }
        format.json { render json: @porterage.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @porterage = Porterage.find(params[:id])

    respond_to do |format|
      format.html
      format.json { render json: @porterage }
    end
  end

  def update
    @porterage = Porterage.find(params[:id])

    respond_to do |format|
      if @location_shed.update_attributes(params[:porterage])
        format.html { render 'index', notice: "Porte Modificado Correctamente" }
        format.json { render json: @porterage, status: :ok }
      else
        format.html { render action: 'edit' }
        format.json { render json: @porterage.errors, status: :unprocessable_entity }
      end
    end
  end

  def search
    res = Porterage.search do
      with :id, params[:code] unless params[:code].blank?
      with(:exit_date).greater_than_or_equal_to(params[:day_from].to_date) unless params[:day_from].blank?
      with(:exit_date).less_than_or_equal_to(params[:day_to].to_date) unless params[:day_to].blank?
      with :origin, params[:origin] unless params[:origin].blank?
      with :destiny, params[:destiny] unless params[:destiny].blank?
      fulltext :model_name unless params[:model_name].blank?
      fulltext(params[:status])

      paginate per_page: 2000
    end

    @porterages = res.results
  end

  def scan_code
    @porterage = Porterage.find(params[:id])
    @material_codes = @porterage.materials

    respond_to do |format|
      format.html
      format.json { render json: @porterage }
    end
  end

  def insert_code
    @porterage = Porterage.find(params[:id])
    material = Material.where("id = #{params[:ean][0..-2].to_i}").first
    material.assign_porterage(@porterage)

    respond_to do |format|
      if material.present?
        format.html { redirect_to scan_code_porterage_path(@porterage), notice: "Material asignado al porte" }
      else
        format.html { render action: 'scan_code', alert: "Hubo un error al asignar el material" }
      end
    end
  end

  def insert_manual_code
    @porterage = Porterage.find(params[:id])
    code = params[:qr].split("-")
    material = Material.where("id = #{code[1].to_i}").first
    @material_codes = @porterage.materials

    respond_to do |format|
      if !material.blank? && material.assign_porterage(@porterage)
        format.html { redirect_to scan_code_porterage_path(@porterage), notice: "Material asignado al porte" }
      else
        format.html { redirect_to scan_code_porterage_path(@porterage), alert: "Hubo un error al asignar el material" }
      end
    end
  end

  def insert_batch_codes
    #TODO limpiar digito de control en carga por bloque
    @porterage = Porterage.find(params[:id])
    codes = Material.limpiar_batch(params[:codegroup])
    materials = Material.where("id in(?)", codes.map { |s| s.to_i })

    @porterage.assign_batch_porterages(materials)

    respond_to do |format|
      format.html { redirect_to scan_code_porterage_path(@porterage), notice: "Material asignado al porte" }
    end
  end

  def delete_code
    porterage = Porterage.find(params[:id])
    material = Material.find(params[:code])

    porterage.materials.delete(material)

    @material_codes = porterage.materials
  end

  def deliver
    @porterage = Porterage.find(params[:id])
    @porterage.update_attribute(:status, Porterage::STATUS_DELIVERED)
    @porterage.update_attribute(:enter_date, Date.today)
    @porterage.materials.each do |material|
      material.assign_porterage(@porterage, "destiny")
    end
  end

  def deliver_back
    @porterage = Porterage.find(params[:id])
    @porterage.update_attribute(:status, Porterage::STATUS_TRANSIT)
    @porterage.update_attribute(:enter_date, nil)
  end
end